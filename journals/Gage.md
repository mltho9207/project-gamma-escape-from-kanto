## FROM SOUP

LOST DUE TO GIT stuff. live and learn...


## THE OTHER DAY

Today was particularly challenging. While working on the Docker Compose file with all its components, I encountered errors that seemed unique to my setup. These issues primarily involved Docker, followed by challenges with psycopg and PostgreSQL's reluctance to communicate with FastAPI. However, the most significant hurdle for me was configuring PgAdmin, which brought everything to a standstill. I couldn't help but feel that I was the cause of these problems.

To address this, Campbell, Matt, and I decided to refactor everything to ensure uniformity and eliminate any anomalies. We were 'mob coding.' Despite the fact that server registration worked smoothly for everyone else, I was confronted with a glaring red error: "UNABLE TO CONNECT TO SERVER: connection failed: FATAL: password authentication failed for user 'myuser'."

With our collective efforts in 'mob coding,' we were able to successfully address this issue and make progress, but I can't help but think that my current issues may have delayed our project.


## DAY 3

required: 'DEAR DIARY'

   Today was significantly better than yesterday. We successfully completed the start-up and began working on our DB Migrations. Campbell played a crucial role in this process by having test inputs ready to go. After that, we focused on the authentication (AUTH) aspect.

    While I was working on the authentication paths, I started considering the need to 'revise' our database. This revision aims to ensure that the data requirements of a specific page are almost entirely fulfilled by a single table. I suspect that this may be an ongoing process, but the initial days of structuring the database definitely provide us with a deeper understanding of why these revisions are necessary.


## THE YESTERDAY

required: 'DEAR DIARY'

    Long day of backend-AUTH, started working on a DB superclass to keep the code pretty DRY moving foreward.


## THE DAY BEFORE THAT

required: 'DEAR DIARY'

    I got a single post to my DB, using the super class. I removed username unique from the DB and it broke the entire thing, and allowed duplicates to even be posted. This probably needs to be shelfed until I better know what all of the typical requests are going to look like.

    Matt ultimatly got the auth working as well using raw insertion. So we will be brainstorming and starting endpoints next.



## THAT ONE TIME...

required: 'DEAR DIARY'

    Today we "AGAIN!" went back to the wireframes to map out the endpoints and re-organize our thought patterns of how everything is 'ideally' working. I've
    been working on the inital server start up, that is sending a couple of API requests to Poke APi. I initally thought this was going to be easy (I was too simplistic in my approach). Then as I conversed with Matt and Campbell, It dawned on me to fetch the first 151 as a limit, that would be a list. Then use those individual url's retured to drill down to the detail. Then from the detail I will be able to use some dictionary magic to get the info we need to process into the database. I mapped out my pseudo code for tomorrow.


## THE DAY AFTER

'DEAR DIARY'

    It took a little longer than expected, but the inital 151 GET requests and a check to make sure it doesn't run anymore on startup was required.
    WOO!



## THE DAY BEFORE

'DEAR DIARY'

    We ran into more docker issues, the bane of my existance and a problem that has persisted throught this course is my battle with the whale.
    The amount of times, in a day I have restart my computer due to docker containers falling dead. It happend 8 times today.


## ANOTHER DAY

'DEAR DIARY'

    Today we started back end AUTH work ups. We are mob coding this. I think this is the best way moving foreward. I made many changes to the structure of our group and how we conduct ourselves through our day with our STAND UP MEETING notes. The repetative asking of what should I be doing? That statement was weighing on me from previous days.


## THIS DAY

'DEAR DIARY'

    The day started off well, we explained our methodology moving foreward to David Sells, and I don't think things really landed home. I think his lack of understanding
    was prohibiting him from actually applying himself to learn. I had a plan to get him caught up, but that would have taken LOTS of work on his part. I don't think he was up for that with the personal things going on in his life.


## IN THE DAYTIME

'DEAR DIARY'

    I've been working on a "super class" for our SQL query's called the SQL superman, however AUTH is starting to drag down the morale. We had to reformat the DB tables due to constraint errors as well as FK errors on our inital large migration.

## 5 DAYS LATER

'DEAR DIARY'

    Today was absolutely a wasted day. Communication problems between David Sells and I took front and center. It required mediation,
    I'm very much over, trying to bend over backwords to help someone who doesn't want it.


## THE DAY AFTER THAT

'DEAR DIARY'

    David Sells dropped. Now we each have a larger workload. But I think we all anticipated it with our 4th's lack of presence.
    We press forward, developing our routes, while David cambell started working our DB into steps for migrations.


## THE DAYS ARE RUNNING TOGETHER!

'DEAR DIARY'

    The inital 151 insertion, is completely done and working everytime. This never gets old to see 151 good status codes on start up.



## I THINK THIS WAS YESTERDAY, or THE DAY BEFORE

'DEAR DIARY'

    Dry'd out back end pages, and kept working on the psycopg and routes.



## BIG MISTAKE

'DEAR DIARY'

    Finished the trainer route, however when thinking about this route I think i want to try and add it to the creation of the account to minimize steps in aggregate.
    helped David Cambell with DB refactor after route done.


## TEAMWORK

'DEAR DIARY'

    Over the weekend, we grouped up and spent some time on the project. We actually got more done on a Saturday in half the time we usually do in a normal day. It made me feel like everyone is really finding their own stride. I don't feel as though I'm alone driving the initive forward any longer. We didn't have a front end, going into the last week. But now we have some working pages.


## THE DAY AFTER

'DEAR DIARY'

    I got the GOTCHA page started and began brainstorming on how to carry those through the gameplay loop, because it was decided not to hold quantity in the DB. why?!
    I also finished up the DELETE pokemon instance route.


## THE DAY AFTER TOMORROW

'DEAR DIARY'

    Yesterday I finished the DELETE poke_instance as well as refactored the DB to CASCADE on delete of that instance, We actually finished all the goals set for in yesterdays STAND UP meeting. Good morale booster, but we still are looking slim on time. I'm really gonna need to pick up the pace.


## A FORTNIGHT AND A DAY

'DEAR DIARY'

    I finished up another route that hadn't been finished yesterday, this was the PUT to trainers currency (I think, this is the one I'll write a unit TEST for)
    Continued on the front end gameplay loop.


## CRUNCH

'DEAR DIARY'

    Ok its CRUNCH time, we are offically ALL on the front end working now. We each have a page we are working on. Yesterday I also ended up locking down the paths with the AUTH.


## THE DAY THAT OTHER ONE

'DEAR DIARY'

    We worked everyday this weekend, we got the Front end auth done. This was really kicking our butts, this is because we were making it harder than it needed to be because I was trying to roll my own auth rather than just use the JWTdown import login.


## WEDNESDAY

'DEAR DIARY'

    I built ALL of the endpoint AXIOS routes for GETS, and the modification services after our dark, spiral of an experience that was SWR. I also finished the signup page, the Gotcha page and started and finished the encounter page. David is really running hard into the profile page, and Matt is the master of the inbetween.  Began the inital layout of the capture page and started to gather my logic from the Gotcha and encounter page to entwine.


## 3 YEARS LATER... j/k!

'DEAR DIARY'

    The decision tree of this Capture page is driving me through a loop itself. Holding the state of 5 different items live, when the page is redirecting on every throw because the conditional check of 3 throws is not being triggered...
    I figured it out, kinda.. by breaking things to the smallest factor. I knew I had all the components that were doing the right things, but trying to develop more than two decisions in a single function was the largest factor of these problems. (woops!)


## TO NUTS

'DEAR DIARY'

    I Think since the start of the project, I worked EVERYDAY minus 1 or 2 days on a weekend. But, ALL that time just melted away. We started deployment yesterday since all of our pages are up. and We started really digging into the styling and CSS. Matt really took charge of the styling! Even though he said he doesn't have an eye for design, he does have an eye for detail. Matt has become a tailwind guru, David took to the SQL and continues to suprise me at how many use effects he can try to use on a page. But all in all, I'm SO happy with what my team and I have accomplished over this time. I'm SUPER proud of the guys, and hope all this hard work helps get them a nice paying job.
