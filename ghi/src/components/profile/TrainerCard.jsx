const TrainerCard = ({ trainer }) => {
    return (
        <div className="border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px] p-4 [box-shadow:0_4px_6px_rgba(0,_0,_0,_0.1)] mb-4">
            <h2>Trainer Name: {trainer.trainer_name}</h2>
            <p>Badge ID: {trainer.badge_id}</p>
            <p>Currency: ฿ {trainer.currency}</p>
        </div>
    )
}

export default TrainerCard
