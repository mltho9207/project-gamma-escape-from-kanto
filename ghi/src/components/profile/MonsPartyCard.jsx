import PokemonInstanceCard from './PokemonInstanceCard'
import { Button } from '@/components/ui/button'
import '../../index.css'


// ... (import statements remain unchanged)

const MonsPartyCard = ({ pokemonInstances, onDelete }) => {
    return (
        <div>
            <h3>Your Party:</h3>
            {pokemonInstances && pokemonInstances.length > 0
                ? pokemonInstances.map((pokemon) =>
                      pokemon && pokemon.pokemon_name !== null ? (
                          <div
                              key={pokemon.poke_instance_id}
                              className="border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px] p-4 [box-shadow:0_4px_6px_rgba(0,_0,_0,_0.1)] mb-4 flex flex-col items-center justify-center"
                          >
                              <PokemonInstanceCard pokemon={pokemon} />
                              <div className="flex justify-end mt-4">
                                  <Button
                                      onClick={() =>
                                          onDelete(pokemon.poke_instance_id)
                                      }
                                      className="bg-[linear-gradient(120deg,_#2c0000,_#f81b1b,_#140000)] rounded-[5px] whitespace-nowrap overflow-hidden overflow-ellipsis min-w-[120px] text-lg"
                                  >
                                      Release
                                  </Button>
                              </div>
                          </div>
                      ) : (
                          <div
                              key={`placeholder-${pokemon.poke_instance_id}`}
                              className="border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px] p-4 [box-shadow:0_4px_6px_rgba(0,_0,_0,_0.1)] mb-4 flex flex-col items-center justify-center"
                          >
                              No Pokémon instances available. Please go to the
                              GOTCHA ZONE to catch a Pokemon
                          </div>
                      )
                  )
                : null}
        </div>
    )
}

export default MonsPartyCard
