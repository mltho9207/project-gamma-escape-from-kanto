
const PokemonInstanceCard = ({ pokemon }) => {
    // Capitalize the first letter of pokemon.name and pokemon.element
    const nameCapitalized =
        pokemon.pokemon_name.charAt(0).toUpperCase() +
        pokemon.pokemon_name.slice(1)
    const elementCapitalized =
        pokemon.element.charAt(0).toUpperCase() + pokemon.element.slice(1)

    return (
        <div>
            <div className="flex">
                <div className="flex items-start relative">
                    <div className="flex-[1] p-4 border-[1px] border-[solid] border-[#8cd4d7] rounded-[5px] mr-10">
                        <h4>
                            Level: {pokemon.lvl} {nameCapitalized}{' '}
                        </h4>
                        <p>
                            HP: {pokemon.hp} DFNS: {pokemon.dfns}
                        </p>
                        <p>
                            ATK: {pokemon.atk} SPD: {pokemon.spd}
                        </p>
                        <p>Type: {elementCapitalized}</p>
                    </div>
                </div>
                <div className="w-[200px] h-[170px] bg-left bg-cover bg-[url('../public/OpenBall.png')]">
                    <div className="flex-[1] w-[100px] h-[100px] ml-20 mt-9">
                        <img
                            src={pokemon.sprite_url_front}
                            alt="Front Sprite"
                            className="object-contain w-full h-full"
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PokemonInstanceCard
