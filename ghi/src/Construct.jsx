//@ts-check
/**
 * @typedef {{module: number, week: number, day: number, min: number, hour: number}} LaunchInfo
 *
 * @param {{info: LaunchInfo | undefined }} props
 * @returns {React.ReactNode}
 */
function Construct(props) {
    if (!props.info) {
        return (
            <div className="App">
                <header className="App-header">
                    <p>Loading...</p>
                    <p>Pour one out for the boys!</p>
                </header>
            </div>
        )
    }

    return <div></div>
}

export default Construct
