import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { getProfileInfo } from '../services/api/getServices'
import GreatBallImage from '@/images/GreatBall_Sprite.png'
import SafariBallImage from '@/images/SafariBall_Sprite.png'
import MasterBallImage from '@/images/MasterBall_Sprite.png'
import PokeBallImage from '@/images/PokeBall_Sprite.png'
import UltraBallImage from '@/images/UltraBall_Sprite.png'
import { Button } from '@/components/ui/button'
import { ChevronDownIcon, ChevronUpIcon } from 'lucide-react'
import {
    Card,
    CardContent,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'
import { updateCurrency } from '@/services/api/modServices'
import { Modal } from '@/components/gotcha/modal'

function GotchaPage() {
    const [showModal, setShowModal] = useState(false)
    const [modalMessage, setModalMessage] = useState('')
    const navigate = useNavigate()
    const [data, setData] = useState(null)
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(true)
    const [balls, setBalls] = useState({
        SafariBall: 12,
        PokeBall: 0,
        GreatBall: 0,
        UltraBall: 0,
        MasterBall: 0,
    })
    const [totalCost, setTotalCost] = useState(0)

    useEffect(() => {
        getProfileInfo()
            .then((response) => {
                setData(response)
                setIsLoading(false)
            })
            .catch((err) => {
                console.error('Failed to fetch profile info:', err)
                setError(
                    'Failed to load profile information. Please try again later.'
                )
                setIsLoading(false)
            })
        // Load saved data from localStorage if it exists
        const savedData = localStorage.getItem('gotchaData')
        if (savedData) {
            const { balls, totalCost } = JSON.parse(savedData)
            setBalls(balls)
            setTotalCost(totalCost)
        }
    }, [])

    const BALL_TYPES = [
        { name: 'SafariBall', cost: 0, image: SafariBallImage },
        { name: 'PokeBall', cost: 10, image: PokeBallImage },
        { name: 'GreatBall', cost: 15, image: GreatBallImage },
        { name: 'UltraBall', cost: 25, image: UltraBallImage },
        { name: 'MasterBall', cost: 60, image: MasterBallImage },
    ]

    const handleIncrement = (ballName) => {
        if (
            ballName !== 'SafariBall' &&
            balls[ballName] < 12 &&
            balls.SafariBall > 0
        ) {
            const cost = BALL_TYPES.find((ball) => ball.name === ballName).cost
            setBalls((prev) => ({
                ...prev,
                [ballName]: prev[ballName] + 1,
                SafariBall: prev.SafariBall - 1,
            }))
            setTotalCost((prev) => prev + cost)
        }
    }

    const handleDecrement = (ballName) => {
        if (ballName !== 'SafariBall' && balls[ballName] > 0) {
            const cost = BALL_TYPES.find((ball) => ball.name === ballName).cost
            setBalls((prev) => ({
                ...prev,
                [ballName]: prev[ballName] - 1,
                SafariBall: prev.SafariBall + 1,
            }))
            setTotalCost((prev) => prev - cost)
        }
    }

    const handleBeginHunt = async () => {
        const userCurrency = data && data[0] ? data[0].currency : 0

        if (userCurrency - totalCost >= 0) {
            try {
                const response = await updateCurrency({ currency: -totalCost })
                localStorage.setItem(
                    'gotchaData',
                    JSON.stringify({ balls, totalCost })
                )
                navigate('/encounter')
            } catch (error) {
                console.error('Error updating currency:', error)
            }
        } else {
            setModalMessage("Oof. You're broke. Safari balls for you...")
            setShowModal(true)
        }
    }

    return (
        <>
            <div className="flex flex-col gap-4 ml-[125px] p-4">
                <div className="flex-grow">
                    <div className="flex flex-col items-center justify-center w-full">
                        <div className="flex flex-row flex-wrap justify-center">
                            <div className="flex flex-col">
                                <div className="py-20">
                                    <Card className="rounded">
                                        <CardHeader>
                                            <CardTitle>
                                                Purchase Balls!
                                            </CardTitle>
                                        </CardHeader>
                                        <CardContent>
                                            <p>
                                                Safari Balls are free to use.
                                                You only get 12 balls per hunt.
                                            </p>
                                        </CardContent>
                                        <CardFooter>
                                            <p>GLHF!</p>
                                        </CardFooter>
                                    </Card>
                                </div>
                                <div className="py-20">
                                    <Card className="rounded">
                                        <CardContent className="flex flex-row flex-wrap justify-center">
                                            {BALL_TYPES.map((ball) => (
                                                <div
                                                    key={ball.name}
                                                    className="m-[10px]"
                                                >
                                                    <img
                                                        src={ball.image}
                                                        alt={ball.name}
                                                    />
                                                    <h3 className="justify-center">
                                                        {ball.name}
                                                    </h3>
                                                    <p>
                                                        Quantity:{' '}
                                                        {balls[ball.name]}
                                                    </p>
                                                    {ball.name !==
                                                        'SafariBall' && (
                                                        <>
                                                            <Button
                                                                variant="outline"
                                                                size="icon"
                                                                onClick={() =>
                                                                    handleIncrement(
                                                                        ball.name
                                                                    )
                                                                }
                                                            >
                                                                <ChevronUpIcon className="h-4 w-4" />
                                                            </Button>
                                                            <Button
                                                                variant="outline"
                                                                size="icon"
                                                                onClick={() =>
                                                                    handleDecrement(
                                                                        ball.name
                                                                    )
                                                                }
                                                            >
                                                                <ChevronDownIcon className="h-4 w-4" />
                                                            </Button>
                                                        </>
                                                    )}
                                                </div>
                                            ))}
                                        </CardContent>
                                        <CardFooter className="flex-row justify-center">
                                            <div>Total Cost: ฿ {totalCost}</div>
                                        </CardFooter>
                                    </Card>
                                    <Button
                                        variant="destructive"
                                        className="w-full rounded"
                                        onClick={handleBeginHunt}
                                    >
                                        Begin the Hunt
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
                <p>{modalMessage}</p>
            </Modal>
        </>
    )
}

export default GotchaPage
