import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from '@/components/ui/button'
import { Input } from '@/components/ui/input'
import { Label } from '@/components/ui/label'
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'
import { createAccount } from '@/services/api/modServices'

const SignupForm = () => {
    const navigate = useNavigate()
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [signupError, setSignupError] = useState('');


    const handleSubmit = async (e) => {
        e.preventDefault()
        const accountData = { username, password }
        const success = await createAccount(accountData)
        if (!success) {
            setSignupError('Unable to create Account. Please try again.');
        } else {
            navigate('/profile')
        }
    };

    return (
        <>
            <div className="flex flex-col gap-4 ml-[125px] p-4">
                <div className="flex-grow">
                    <div className="flex flex-col items-center justify-center w-full">
                        <Card className="w-[300px]">
                            <CardHeader>
                                <CardTitle>Sign Up!</CardTitle>
                                <CardDescription>
                                    BECOME ONE OF US
                                </CardDescription>
                            </CardHeader>
                            <CardContent>
                                {signupError && <div className="text-red-500 mb-8">{signupError}</div>}
                                <form onSubmit={(e) => handleSubmit(e)}>
                                    <div className="grid w-full items-center gap-4">
                                        <div className="flex flex-col space-y-1.5">
                                            <Label htmlFor="name">
                                                Username
                                            </Label>
                                            <Input
                                                required
                                                placeholder="Username"
                                                name="username"
                                                type="text"
                                                className="form-control rounded"
                                                minLength="4"
                                                maxLength="20"
                                                onChange={(e) =>
                                                    setUsername(e.target.value)
                                                }
                                            />
                                        </div>
                                        <div className="flex flex-col space-y-1.5">
                                            <Label htmlFor="name">
                                                Password
                                            </Label>
                                            <Input
                                                required
                                                placeholder="Password"
                                                name="password"
                                                type="password"
                                                minLength="6"
                                                maxLength="20"
                                                className="form-control rounded"
                                                onChange={(e) =>
                                                    setPassword(e.target.value)
                                                }
                                            />
                                        </div>
                                        <div className="flex justify-center rounded">
                                            <Button
                                                type="submit"
                                                className="rounded"
                                            >
                                                Sign Up!
                                            </Button>
                                        </div>
                                    </div>
                                </form>
                            </CardContent>
                            <CardFooter className="flex justify-center"></CardFooter>
                        </Card>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SignupForm
