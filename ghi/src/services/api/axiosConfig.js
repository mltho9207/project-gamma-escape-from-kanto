import axios from 'axios'

const API_HOST = import.meta.env.VITE_API_HOST

const axiosInstance = axios.create({
    baseURL: API_HOST,
    withCredentials: true,
})

export default axiosInstance
