from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
    HTTPException,
    status,
)
from models import (
    AccountToken,
    AccountIn,
    AccountOut,
    AccountForm,
    Trainers,
)
from queries.accounts import AccountsQueries, DuplicateAccountError
from queries.trainers import TrainersQueries
from authenticator import authenticator
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        token = AccountToken(
            access_token=request.cookies[authenticator.cookie_name],
            type="Bearer",
            account=account,
        )
        return token


@router.post("/api/accounts", response_model=AccountToken)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountsQueries = Depends(),
    trainers_repo: TrainersQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
        if account:
            trainer_data = Trainers(
                name=account.username,
                currency=500,
                badge_id=1,
                accounts_id=account.id,
            )
            await trainers_repo.create_trainer(trainer_data)
            print("trainer data from create_account:", trainer_data)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    oauth_form = OAuth2PasswordRequestForm(
        username=form.username, password=form.password, scope=""
    )
    token = await authenticator.login(response, request, oauth_form, repo)
    account_out = AccountOut(**account.dict())
    return AccountToken(account=account_out, **token.dict(), type="Bearer")
