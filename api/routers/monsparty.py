from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator
from models import MonsParty
from queries.monsparty import MonsPartyQueries
from typing import List

router = APIRouter()


@router.post(
    "/api/pokemon/party/add-pokemon-to-party",
    response_model=MonsParty,
)
async def add_pokeinstance_to_party(
    poke_instance_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    mons_party_queries: MonsPartyQueries = Depends(),
):
    trainer_id = account_data["id"]
    try:
        result = await mons_party_queries.add_pokeinstance_to_party(
            trainer_id, poke_instance_id
        )
        print("Result:", result)
        if result:
            return result
        elif result is None:
            raise HTTPException(
                status.HTTP_202_ACCEPTED,
                detail="The party is already full. Cannot add more Pokemon.",
            )
    except HTTPException as e:
        print(f"Status code: {e.status_code}")
        raise e


@router.put("/api/pokemon/party/update-battle-order")
async def change_battle_order(
    new_order: List[int],
    account_data: dict = Depends(authenticator.get_current_account_data),
    mons_party_queries: MonsPartyQueries = Depends(),
):
    trainers_id = account_data["id"]
    await mons_party_queries.update_battle_order(trainers_id, new_order)
    return {
        "message": f"Successfully updated battle order Trainer {trainers_id}!"
    }
