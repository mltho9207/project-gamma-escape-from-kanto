steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS poke_instances (
            poke_instance_id  SERIAL  UNIQUE  NOT NULL,
            pokedex_id int NOT NULL,
            name varchar(20)  NOT NULL,
            lvl int  NOT NULL,
            hp int  NOT NULL,
            dfns int  NOT NULL,
            atk int  NOT NULL,
            spd int  NOT NULL,
            element varchar(20)  NOT NULL,
            caught boolean  NOT NULL,
            sprite_url_front varchar(200)  NOT NULL,
            sprite_url_back varchar(200)  NOT NULL,
            helm_id int  NULL,
            chest_id int  NULL,
            boots_id int  NULL,
            weapons_id int  NULL,
            special_moves_id int  NULL,
            wins int  NULL,
            CONSTRAINT pk_poke_instances PRIMARY KEY (poke_instance_id),
            CONSTRAINT fk_poke_instances_pokedex_id
                FOREIGN KEY(pokedex_id)
                REFERENCES pokedex (id),
            CONSTRAINT fk_poke_instances_helm_id
                FOREIGN KEY(helm_id)
                REFERENCES helms (id),
            CONSTRAINT fk_poke_instances_chest_id
                FOREIGN KEY(chest_id)
                REFERENCES armor (id),
            CONSTRAINT fk_poke_instances_boots_id
                FOREIGN KEY(boots_id)
                REFERENCES boots (id),
            CONSTRAINT fk_poke_instances_weapons_id
                FOREIGN KEY(weapons_id)
                REFERENCES weapons (id),
            CONSTRAINT fk_poke_instances_special_moves_id
                FOREIGN KEY(special_moves_id)
                REFERENCES special_moves (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS poke_instances;
        """,
    ]
]
