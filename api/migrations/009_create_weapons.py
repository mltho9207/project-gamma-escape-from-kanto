steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS weapons (
            id  SERIAL  UNIQUE  NOT NULL,
            name varchar(20)  NOT NULL,
            atk int  NOT NULL,
            durability int  NOT NULL,
            max_durability int  NOT NULL,
            CONSTRAINT pk_weapon PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS weapons;
        """,
    ]
]
