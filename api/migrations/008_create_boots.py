steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS boots (
            id  SERIAL  UNIQUE  NOT NULL,
            name varchar(20)  NOT NULL,
            def int  NOT NULL,
            durability int  NOT NULL,
            max_durability int  NOT NULL,
            CONSTRAINT pk_boots PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS boots;
        """,
    ]
]
