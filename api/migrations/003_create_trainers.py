steps = [
    [
        # Up migration
        """
        CREATE TABLE IF NOT EXISTS trainers (
            id SERIAL UNIQUE NOT NULL,
            name varchar(20) NOT NULL,
            currency float NOT NULL,
            badge_id int NULL,
            accounts_id int NULL,
            CONSTRAINT pk_trainers PRIMARY KEY (id),
            CONSTRAINT fk_trainers_badge_id
                FOREIGN KEY(badge_id)
                REFERENCES badges (id),
            CONSTRAINT fk_trainers_accountsid
                FOREIGN KEY (accounts_id)
                REFERENCES accounts (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS trainers;
        """,
    ]
]
