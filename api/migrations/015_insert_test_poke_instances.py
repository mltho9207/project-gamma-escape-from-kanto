steps = [
    [  # Data Up migration
        """
        INSERT INTO poke_instances (
            name,
            lvl,
            hp,
            dfns,
            atk,
            spd,
            element,
            caught,
            sprite_url_front,
            sprite_url_back,
            pokedex_id
        )
        SELECT
            name,
            lvl,
            hp,
            dfns,
            atk,
            spd,
            element,
            true,
            sprite_url_front,
            sprite_url_back,
            id
        FROM pokedex
        WHERE id = ANY(ARRAY[2, 50, 137]);
        """,
        # Data Down migration
        """
        -- Delete records from poke_instances based on specific pokedex_ids
        DELETE FROM poke_instances WHERE id = ANY(ARRAY[2, 50, 137]);
        """,
    ],
]
