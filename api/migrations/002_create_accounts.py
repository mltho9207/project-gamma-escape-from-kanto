steps = [
    [
        # Up migration
        """
        CREATE TABLE IF NOT EXISTS accounts (
            id  SERIAL UNIQUE  NOT NULL,
            username varchar(20) UNIQUE NOT NULL,
            hashed_password varchar(255)  NOT NULL,
            CONSTRAINT pk_accounts PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS accounts;
        """,
    ]
]
