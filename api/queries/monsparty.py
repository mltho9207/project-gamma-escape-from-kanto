from queries.client import get_api_connection
from fastapi import HTTPException, status
from psycopg.rows import dict_row
from typing import List


class MonsPartyQueries:
    async def add_pokeinstance_to_party(
        self, trainer_id: int, poke_instance_id: int
    ):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    # Check if the trainer exists
                    trainer_query = "SELECT id FROM trainers WHERE id = %s;"
                    await cur.execute(trainer_query, (trainer_id,))
                    trainer_data = await cur.fetchone()

                    if not trainer_data:
                        raise HTTPException(
                            status_code=400, detail="Trainer does not exist"
                        )

                    party_check_query = """
                        SELECT COUNT(*) AS count
                        FROM mons_party
                        WHERE trainers_id = %s AND poke_instance_id = %s;
                    """
                    await cur.execute(
                        party_check_query, (trainer_id, poke_instance_id)
                    )
                    party_check_result = await cur.fetchone()

                    if (
                        party_check_result is not None
                        and party_check_result["count"] > 0
                    ):
                        raise HTTPException(
                            status_code=400,
                            detail="Pokemon is already in the party.",
                        )

                    party_query = """
                        SELECT COUNT(*) AS party
                        FROM mons_party
                        WHERE trainers_id = %s;
                    """

                    await cur.execute(party_query, (trainer_id,))
                    party = await cur.fetchone()

                    if party is None:
                        raise HTTPException(
                            status.HTTP_418_IM_A_TEAPOT,
                            detail="""
                            If you are reading this,
                            you are
                            ... Short and Stout
                            """,
                        )

                    if party["party"] < 3:
                        next_battle_order = party["party"] + 1
                        # Insert the Pokemon into the party
                        insert_query = """
                            INSERT INTO mons_party (
                                trainers_id,
                                poke_instance_id,
                                battle_order
                            )
                            VALUES (%s, %s, %s)
                            RETURNING *;
                        """
                        await cur.execute(
                            insert_query,
                            (trainer_id, poke_instance_id, next_battle_order),
                        )
                        mons_party = await cur.fetchone()
                        return mons_party
                    elif party["party"] > 3:
                        raise HTTPException(
                            status.HTTP_202_ACCEPTED,
                            detail="The party is already full.",
                        )
        except Exception as e:
            raise e

    async def update_battle_order(
        self, trainers_id: int, new_order: List[int]
    ):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    # Get a total count of the party size
                    count_party = """
                            SELECT COUNT(*)
                            FROM mons_party
                            WHERE trainers_id = %s;
                    """
                    await cur.execute(count_party, (trainers_id,))
                    count_result = await cur.fetchone()
                    # Handle a case that shouldn't happen (party is 0)
                    if count_result is None:
                        raise HTTPException(
                            status.HTTP_418_IM_A_TEAPOT,
                            detail="""
                            If you are reading this,
                            you are... Short and Stout
                            """,
                        )
                    # Assign the value in the COUNT column to party_size
                    party_size = count_result["count"]

                    if party_size >= 2:
                        # get all of the rows of the trainer's party
                        party_query = """
                            SELECT *
                            FROM mons_party
                            WHERE trainers_id = %s;
                        """
                        await cur.execute(party_query, (trainers_id,))
                        rows = await cur.fetchall()

                        # Iterate over the rows and update each one
                        for i, row in enumerate(rows):
                            swap_query = """
                                UPDATE mons_party
                                SET battle_order = %s
                                WHERE id = %s;
                            """
                            await cur.execute(
                                swap_query, (new_order[i], row["id"])
                            )
                    else:
                        raise HTTPException(
                            status.HTTP_202_ACCEPTED,
                            detail="You don't have enough Pokemon to swap!",
                        )
        except Exception as e:
            raise e
