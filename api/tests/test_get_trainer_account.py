from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from models import AccountOut
from queries.trainers import TrainersQueries

# Initialize the TestClient with the FastAPI application
client = TestClient(app)


# Mock the authenticator.get_current_account_data function
def mock_get_current_account_data():
    return {"id": 42, "username": "MilesDavis"}


# Mock the TrainersQueries class to simulate database interaction
class MockTrainersQueries:
    async def get_trainer_info(self, account_id: int):
        return AccountOut(id=42, username="MilesDavis")


# Unit test for the get_account_info endpoint


def test_get_account_info():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[TrainersQueries] = MockTrainersQueries

    # Act
    # Make a GET request to the endpoint
    response = client.get("/api/trainers/account/mine")

    # Assert
    # Check that the status code is  200 (OK)
    assert response.status_code == 200
    # Check that the JSON response matches the expected output
    assert response.json() == {"id": 42, "username": "MilesDavis"}
