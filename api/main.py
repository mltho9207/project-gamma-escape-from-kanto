from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from queries.client import (
    get_api_connection,
    is_pokedex_populated,
    close_async_pool,
)
from queries.pokeapi import insertion
from authenticator import authenticator
from routers import accounts, pokemon, utility, monsparty, profilepage, trainer
import os


app = FastAPI()

# Define the allowed origins for production
origins = [
    "http://localhost:5173",
    "http://localhost:8000",
    "https://oct-2023-12-et-api.mod3projects.com",
    "https://project-gamma-ram-rod-c9920e4a8fa23115460a5919cbdb3a824d514d9a5.gitlab.io",
]

# Configure CORS for production
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Include routers
app.include_router(authenticator.router, tags=["Auth"])
app.include_router(accounts.router, tags=["Auth"])
app.include_router(utility.router, tags=["Utility"])
app.include_router(pokemon.router, tags=["Pokemon"])
app.include_router(monsparty.router, tags=["Pokemon"])
app.include_router(profilepage.router, tags=["Trainer"])
app.include_router(trainer.router, tags=["Trainer"])

"""
To Deploy:
1. In api/main.py:
    - change 'if os.environ.get("DEV_ENV") == 'True':' to 'False'

2. In ghi/app.jsx:
    - uncomment 'const domain' and 'const baseUrl' and change
    - <AuthProvider baseUrl={API_HOST}> to <AuthProvider baseUrl={baseUrl}>

3. In Top level .env:
    - change DEV_ENV=True to DEV_ENV=False
"""
if os.environ.get("DEV_ENV") == "True":
    @app.on_event("startup")
    async def startup_event():
        async with get_api_connection() as conn:
            if not await is_pokedex_populated(conn):
                await insertion()


@app.on_event("shutdown")
async def shutdown_event():
    await close_async_pool()
