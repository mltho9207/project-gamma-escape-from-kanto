from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import Optional, List


class AccountIn(BaseModel):
    username: str
    password: str


class AccountOutWithHash(BaseModel):
    id: int
    username: str
    hashed_password: str


class AccountOut(BaseModel):
    id: int
    username: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut
    access_token: str
    type: str


class HttpError(BaseModel):
    detail: str


class Ball(BaseModel):
    id: int
    ball_type: str
    cost: float
    catch_rate: float


class Pokedex(BaseModel):
    id: int
    name: str
    lvl: int
    hp: int
    dfns: int
    atk: int
    spd: int
    element: str
    caught: bool
    sprite_url_front: str
    sprite_url_back: str


class Trainers(BaseModel):
    name: str
    currency: float
    badge_id: int | None = None
    accounts_id: int


class CatchRequest(BaseModel):
    pokemon_id: int
    ball_type: str


"""
There are optional data types here because they are needed to be able to
write the pokemon info into the table. The optional stuff can be added
later with another query I think.
"""


class PokedexToPokeinstance(BaseModel):
    poke_instance_id: int
    name: str
    lvl: int
    hp: int
    dfns: int
    atk: int
    spd: int
    element: str
    caught: bool
    sprite_url_front: str
    sprite_url_back: str
    helm_id: Optional[int]
    chest_id: Optional[int]
    boots_id: Optional[int]
    weapons_id: Optional[int]
    special_moves_id: Optional[int]
    wins: Optional[int]


class MonsParty(BaseModel):
    id: int
    trainers_id: int
    poke_instance_id: int
    battle_order: Optional[int]


class ProfileInfo(BaseModel):
    trainers_id: int
    trainer_name: str
    badge_id: int
    currency: float
    battle_order: Optional[int]
    poke_instance_id: Optional[int]
    pokemon_name: Optional[str]
    lvl: Optional[int]
    hp: Optional[int]
    dfns: Optional[int]
    atk: Optional[int]
    spd: Optional[int]
    element: Optional[str]
    sprite_url_front: Optional[str]
    sprite_url_back: Optional[str]


class ProfilePageResponse(BaseModel):
    __root__: List[ProfileInfo]


class UpdateTrainerCurrency(BaseModel):
    currency: float


class RandomEncounter(BaseModel):
    id: int
    name: str
    lvl: int
    hp: int
    dfns: int
    atk: int
    spd: int
    element: str
    sprite_url_front: str
    sprite_url_back: str
